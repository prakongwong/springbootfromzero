package com.piyapat.springboot.SpringbootFromZero;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootFromZeroApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootFromZeroApplication.class, args);
	}


}
